package handlers

import (
	"fmt"
	"go-backend/models"
	"go-backend/serializers"
	"net/http"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type ReportHandler struct {
	DB *gorm.DB
}

var reportJSON struct {
	Title            string `json:"title"`
	Description      string `json:"description"`
	MeetupID         uint   `json:"meetupID"`
	SpeakerID        uint   `json:"speakerID"`
	Order            int    `json:"order"`
	PresentationLink string `json:"presentationLink"`
	VideoLink        string `json:"videoLink"`
}

func (h *ReportHandler) GetReports(c *gin.Context) {
	var reports []models.Report

	if err := h.DB.Preload("Meetup").Preload("Speaker").Find(&reports).Error; err != nil {
		fmt.Println("Error fetching reports:", err)
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to fetch reports"})
		return
	}

	// Serialize the reports
	var serializedReports []serializers.ReportSerializer
	for _, report := range reports {
		serializedReport := serializers.NewReportSerializer(report)
		serializedReports = append(serializedReports, serializedReport)
	}

	c.JSON(http.StatusOK, serializedReports)
}

func (h *ReportHandler) CreateReport(c *gin.Context) {

	if err := c.ShouldBindJSON(&reportJSON); err != nil {
		fmt.Println("Error binding JSON:", err)
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid JSON data"})
		return
	}

	// Fetch Meetup and Speaker based on IDs
	var meetup models.Meet
	var speaker models.Speaker

	if err := h.DB.First(&meetup, reportJSON.MeetupID).Error; err != nil {
		fmt.Println("Error fetching Meetup:", err)
		c.JSON(http.StatusNotFound, gin.H{"error": "Meetup not found"})
		return
	}

	if err := h.DB.First(&speaker, reportJSON.SpeakerID).Error; err != nil {
		fmt.Println("Error fetching Speaker:", err)
		c.JSON(http.StatusNotFound, gin.H{"error": "Speaker not found"})
		return
	}

	// Create the Report struct with associated Meetup and Speaker
	report := models.Report{
		Title:            reportJSON.Title,
		Description:      reportJSON.Description,
		MeetupID:         reportJSON.MeetupID,
		Meetup:           meetup,
		SpeakerID:        reportJSON.SpeakerID,
		Speaker:          speaker,
		Order:            reportJSON.Order,
		PresentationLink: reportJSON.PresentationLink,
		VideoLink:        reportJSON.VideoLink,
	}

	if err := h.DB.Create(&report).Error; err != nil {
		fmt.Println("Error creating report:", err)
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to create report"})
		return
	}

	serializedReport := serializers.NewReportSerializer(report)
	c.JSON(http.StatusCreated, serializedReport)
}

func (h *ReportHandler) UpdateReport(c *gin.Context) {
	reportID := c.Param("id")
	var existingReport models.Report
	if err := h.DB.Preload("Meetup").Preload("Speaker").Find(&existingReport, reportID).Error; err != nil {
		fmt.Println("Error fetching reports:", err)
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to fetch reports"})
		return
	}

	if err := c.ShouldBindJSON(&reportJSON); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	var meetup models.Meet
	var speaker models.Speaker

	if err := h.DB.First(&meetup, reportJSON.MeetupID).Error; err != nil {
		fmt.Println("Error fetching Meetup:", err)
		c.JSON(http.StatusNotFound, gin.H{"error": "Meetup not found"})
		return
	}

	if err := h.DB.First(&speaker, reportJSON.SpeakerID).Error; err != nil {
		fmt.Println("Error fetching Speaker:", err)
		c.JSON(http.StatusNotFound, gin.H{"error": "Speaker not found"})
		return
	}

	existingReport.Title = reportJSON.Title
	existingReport.Description = reportJSON.Description
	existingReport.MeetupID = reportJSON.MeetupID
	existingReport.Meetup = meetup
	existingReport.SpeakerID = reportJSON.SpeakerID
	existingReport.Speaker = speaker
	existingReport.Order = reportJSON.Order
	existingReport.PresentationLink = reportJSON.PresentationLink
	existingReport.VideoLink = reportJSON.VideoLink

	if err := h.DB.Save(&existingReport).Error; err != nil {
		fmt.Println("Error updating report:", err)
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to update report"})
		return
	}

	if err := h.DB.Preload("Meetup").Preload("Speaker").Find(&existingReport, reportID).Error; err != nil {
		fmt.Println("Error fetching reports:", err)
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to fetch reports"})
		return
	}

	serializedReport := serializers.NewReportSerializer(existingReport)
	c.JSON(http.StatusOK, serializedReport)
}

func (h *ReportHandler) DeleteReport(c *gin.Context) {
	var report models.Report
	reportID := c.Param("id")

	if err := h.DB.First(&report, reportID).Error; err != nil {
		c.JSON(http.StatusNotFound, gin.H{"error": "Report not found"})
		return
	}

	h.DB.Delete(&report)

	c.JSON(http.StatusNoContent, gin.H{})
}

func (h *ReportHandler) GetReport(c *gin.Context) {
	reportID := c.Param("id")
	var report models.Report
	if err := h.DB.Preload("Meetup").Preload("Speaker").First(&report, reportID).Error; err != nil {
		c.JSON(http.StatusNotFound, gin.H{"error": "Report not found"})
		return
	}
	report_serializer := serializers.NewReportSerializer(report)
	c.JSON(http.StatusOK, report_serializer)
}
