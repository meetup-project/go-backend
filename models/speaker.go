package models

import "gorm.io/gorm"

type Speaker struct {
	gorm.Model
	ID          uint `gorm:"PrimaryKey"`
	LastName    string
	FirstName   string
	Surname     string
	Photo       string
	SpeakerInfo string
}

func (s *Speaker) FullName() string {
	return s.LastName + " " + s.FirstName + " " + s.Surname
}
