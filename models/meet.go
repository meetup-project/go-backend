package models

import (
	"time"

	"gorm.io/gorm"
)

type Meet struct {
	gorm.Model
	ID                uint `gorm:"PrimaryKey"`
	Title             string
	Description       string
	Date              time.Time
	BroadcastLink     string
	BroadcastLogin    string
	BroadcastPassword string
	CountListener     int
	RegistrationForm  string
}

type QuizImage struct {
	gorm.Model
	Images string `json:"images"`
	Meetup uint   `json:"meetup"`
}
