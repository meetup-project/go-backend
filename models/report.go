package models

import (
	"gorm.io/gorm"
)

type Report struct {
	gorm.Model
	ID               uint `gorm:"PrimaryKey"`
	Title            string
	Description      string
	MeetupID         uint
	Meetup           Meet `gorm:"foreignKey:MeetupID;references:ID"`
	SpeakerID        uint
	Speaker          Speaker `gorm:"foreignKey:SpeakerID;references:ID"`
	Order            int
	PresentationLink string
	VideoLink        string
}
