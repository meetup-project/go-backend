package serializers

import (
	"go-backend/models"
)

type SpeakerSerializer struct {
	ID          uint   `json:"id"`
	LastName    string `json:"lastName"`
	FirstName   string `json:"firstName"`
	Surname     string `json:"surname"`
	Photo       string `json:"photo"`
	SpeakerInfo string `json:"speakerInfo"`
	FullName    string `json:"fullName"`
}

func NewSpeakerSerializer(speaker models.Speaker) SpeakerSerializer {
	return SpeakerSerializer{
		ID:          speaker.ID,
		LastName:    speaker.LastName,
		FirstName:   speaker.FirstName,
		Surname:     speaker.Surname,
		Photo:       speaker.Photo,
		SpeakerInfo: speaker.SpeakerInfo,
		FullName:    speaker.FullName(),
	}
}

type SpeakerDetailsSerializer struct {
	ID          uint   `json:"id"`
	LastName    string `json:"lastName"`
	FirstName   string `json:"firstName"`
	Surname     string `json:"surname"`
	Photo       string `json:"photo"`
	SpeakerInfo string `json:"speakerInfo"`
	FullName    string `json:"fullName"`
	Reports []ReportSerializer `json:"reports"`
}

func NewSpeakerReportsSerializer(speaker models.Speaker, reports []models.Report) SpeakerDetailsSerializer {
	var reportSerializers []ReportSerializer
	for _, report := range reports {
		reportSerializers = append(reportSerializers, NewReportSerializer(report))
	}

	return SpeakerDetailsSerializer{
		ID:          speaker.ID,
		LastName:    speaker.LastName,
		FirstName:   speaker.FirstName,
		Surname:     speaker.Surname,
		Photo:       speaker.Photo,
		SpeakerInfo: speaker.SpeakerInfo,
		FullName:    speaker.FullName(),
		Reports: reportSerializers,
	}
}
