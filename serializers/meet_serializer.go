package serializers

import (
	"go-backend/models"
	"time"
)

type MeetSerializer struct {
	ID                uint      `json:"id"`
	Title             string    `json:"title"`
	Description       string    `json:"description"`
	Date              time.Time `json:"date"`
	BroadcastLink     string    `json:"broadcastLink"`
	BroadcastLogin    string    `json:"broadcastLogin"`
	BroadcastPassword string    `json:"broadcastPassword"`
	CountListener     int       `json:"countListener"`
	RegistrationForm  string    `json:"registrationForm"`
}

func NewMeetSerializer(meet models.Meet) MeetSerializer {
	return MeetSerializer{
		ID:                meet.ID,
		Title:             meet.Title,
		Description:       meet.Description,
		Date:              meet.Date,
		BroadcastLink:     meet.BroadcastLink,
		BroadcastLogin:    meet.BroadcastLogin,
		BroadcastPassword: meet.BroadcastPassword,
		CountListener:     meet.CountListener,
		RegistrationForm:  meet.RegistrationForm,
	}
}



type MeetShortSerializer struct {
	ID    uint   `json:"id"`
	Title string `json:"title"`
}

func NewMeetShortSerializer(meet models.Meet) MeetShortSerializer {
	return MeetShortSerializer{
		ID:    meet.ID,
		Title: meet.Title,
	}
}

type MeetDetailsSerializer struct {
	ID                uint      `json:"id"`
	Title             string    `json:"title"`
	Description       string    `json:"description"`
	Date              time.Time `json:"date"`
	BroadcastLink     string    `json:"broadcastLink"`
	BroadcastLogin    string    `json:"broadcastLogin"`
	BroadcastPassword string    `json:"broadcastPassword"`
	CountListener     int       `json:"countListener"`
	RegistrationForm  string    `json:"registrationForm"`
	Reports []ReportSerializer `json:"reports"`
}

func MeetDetailsSerializerHandler(meet models.Meet, allReports []models.Report) MeetDetailsSerializer {
	var meetReports []ReportSerializer
	for _, report := range allReports {
		meetReports = append(meetReports, NewReportSerializer(report))
	}
	return MeetDetailsSerializer{
		ID:                meet.ID,
		Title:             meet.Title,
		Description:       meet.Description,
		Date:              meet.Date,
		BroadcastLink:     meet.BroadcastLink,
		BroadcastLogin:    meet.BroadcastLogin,
		BroadcastPassword: meet.BroadcastPassword,
		CountListener:     meet.CountListener,
		RegistrationForm:  meet.RegistrationForm,
		Reports: meetReports,
	}
}
