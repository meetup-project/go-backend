## Зависимости

Требуется установить go версии 1.21.4 и выше: https://go.dev/dl/

## Настройка проекта

```sh
go get .
```

### Запуск сервера

```sh
go run .
```

Приложение будет доступно на http://localhost:8000/

В main.go описаны эндпоинты для обращения к серверу
